### 1. For running Spark Program:

> (local) ssh -i ~/.ssh/id_rsa -L 30021:localhost:30021 ragarwal@hadoop2.yandex.ru


> (server) PYSPARK_DRIVER_PYTHON=jupyter PYSPARK_PYTHON=/usr/bin/python2 PYSPARK_DRIVER_PYTHON_OPTS='notebook --ip="*" --port=30021 --NotebookApp.token="TOKEN" --no-browser' pyspark2 --master=yarn --num-executors=2

Open browser: localhost:30021 and run the notebook SGA - Spark.ipynb

### 2. For running Spark DF Program:

> (local) ssh -i ~/.ssh/id_rsa -L 30021:localhost:30021 ragarwal@hadoop2.yandex.ru


> (server) PYSPARK_DRIVER_PYTHON=jupyter PYSPARK_PYTHON=/usr/bin/python2 PYSPARK_DRIVER_PYTHON_OPTS='notebook --ip="*" --port=30021 --NotebookApp.token="TOKEN" --no-browser' pyspark2 --master=yarn --num-executors=2

Open browser: localhost:30021 and run the notebook SGA - SparkDF.ipynb

### 3. For Running Hive:

> (local) ssh -i ~/.ssh/id_rsa -L 30021:localhost:30021 ragarwal@hadoop2.yandex.ru

> (server) hive

> Copy paste the hive.sql file or run the file


### 4. For running MR:

> (local) ssh -i ~/.ssh/id_rsa -L 30021:localhost:30021 ragarwal@hadoop2.yandex.ru

> (server) sh test_s.sh

use ragarwal;

DROP TABLE IF EXISTS clickstream;
CREATE EXTERNAL TABLE IF NOT EXISTS clickstream(
    user_id int,
    session_id int,
    event_type STRING,
    event_page STRING,
    timestamp int
)
ROW FORMAT DELIMITED
FIELDS TERMINATED BY '\t'
STORED AS TEXTFILE LOCATION '/data/lsml/sga/'
TBLPROPERTIES ("skip.header.line.count" = "1");
;

SELECT
    *
FROM clickstream
LIMIT
    10;

WITH error_data AS (
    SELECT
        user_id,
        session_id,
        MIN(timestamp) AS break_timestamp
    FROM clickstream
    WHERE
        event_type LIKE '%error%'
    GROUP BY
        user_id,
        session_id
),
joined AS (
    SELECT
        A.*,
        COALESCE(B.break_timestamp, 99999999999999) AS break_timestamp
    FROM clickstream A
    LEFT JOIN error_data B
        ON A.user_id = B.user_id
        AND A.session_id = B.session_id
),
valid_data AS (
    SELECT
        *
    FROM joined
    WHERE
        timestamp < break_timestamp
),
get_arr AS (
    SELECT
        user_id,
        session_id,
        collect_list(CASE WHEN event_page = lag_number THEN NULL ELSE event_page END) AS route_arr
    FROM (
        SELECT
            user_id,
            session_id,
            event_page,
            timestamp,
            LAG(event_page) OVER (
                PARTITION BY
                    user_id,
                    session_id
                ORDER BY
                    timestamp
            ) lag_number
        FROM valid_data
        DISTRIBUTE BY
            user_id,
            session_id
        SORT BY
            timestamp
    ) s
    GROUP BY
        user_id,
        session_id
),
get_str AS (
    SELECT
        concat_ws("-", route_arr) AS route_str
    FROM get_arr
)
SELECT
    route_str,
    COUNT(1) cnt
FROM get_str
GROUP BY
    route_str
ORDER BY
    cnt DESC
LIMIT
    40;

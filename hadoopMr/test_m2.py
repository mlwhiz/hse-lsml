#!/usr/bin/env python

import sys

for line in sys.stdin:
    rec = line.strip().split("|")
    print "%s\t%s" % (rec[0],str(rec[1]))

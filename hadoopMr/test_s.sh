#!/usr/bin/env bash

IN_DIR="/data/lsml/sga/clickstream.csv"
OUT_DIR="out"
OUT_2_DIR="out2"

NUM_REDUCERS=8

hadoop fs -rm -r -skipTrash ${OUT_DIR}

yarn jar /opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-streaming.jar \
    -D mapreduce.job.name="SGA" \
    -D mapreduce.job.reduces=$NUM_REDUCERS \
    -files test_m.py,test_r.py \
    -mapper ./test_m.py \
    -reducer ./test_r.py \
    -input ${IN_DIR} \
    -output ${OUT_DIR}

hadoop fs -rm -r -skipTrash ${OUT_2_DIR}

yarn jar /opt/cloudera/parcels/CDH/lib/hadoop-mapreduce/hadoop-streaming.jar \
    -D mapreduce.job.name="SGA2" \
    -D mapreduce.job.reduces=1 \
    -files test_m2.py,test_r2.py \
    -mapper ./test_m2.py \
    -reducer ./test_r2.py \
    -input ${OUT_DIR} \
    -output ${OUT_2_DIR}

hadoop fs -cat ${OUT_2_DIR}/part-00000 | sort -k 2 -r -n | head -40

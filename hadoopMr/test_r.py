#!/usr/bin/env python

import sys
current_key = None
key = None
keeper = []

def collect_main(y):
    y = list(y)
    try:
        y = sorted(y,key=lambda x: int(x[2]))
    except:
        print(y)
    out = ["START"]
    for elem in y:
        if 'error' in elem[0]:
            return "-".join(out[1:])
        elif out[-1]!=elem[1]:
            out.append(elem[1])

    return "-".join(out[1:])

for line in sys.stdin:
    line = line.strip()
    rec = line.split('\t')
    key = rec[0]
    value = rec[1].split("%|%")
    if current_key == key:
        keeper += [value]
    else:
        if current_key:
            print "%s|%s" %(collect_main(keeper),str(1))
        current_key = key
        keeper = [value]

if current_key == key:
    print "%s|%s" %(collect_main(keeper),str(1))

#!/usr/bin/env python

import sys
from datetime import datetime

for line in sys.stdin:
    rec = line.strip().split("\t")
    user_id,session_id,event_type,event_page,timestamp = rec
    if timestamp!='timestamp':
        print "%s\t%s" % (user_id+"|"+session_id,(event_type+"%|%"+event_page+"%|%"+timestamp))
